# Función que suma dos enteros
def funcion1(num1, num2):
    return num1 + num2
    pass
    
# Función que multiplica dos números decimales
def funcion2(num1, num2):
    return num1 * num2
    pass


# Función que concatena dos cadenas de texto
def funcion3(str1, str2):
    return str1 + str2
    pass


# Función que verifica si un número entero es par
def funcion4(numero):
    return numero%2 == 0
    pass


# Función que calcula el área de un triángulo dado su base y altura
def funcion5(base, altura):
    return 0.5 * base * altura
    pass
